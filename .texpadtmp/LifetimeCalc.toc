\contentsline {section}{Preface}{i}{Doc-Start}
\contentsline {section}{Abstract}{i}{Doc-Start}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Supersymmetry}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Symmetries}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}MSSM}{2}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Organization Principle}{3}{subsubsection.2.2.1}
\contentsline {subsection}{\numberline {2.3}Supersymmetry Breaking}{4}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}EW symmetry breaking in supersymmetry}{4}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Supergravity}{5}{subsection.2.5}
\contentsline {section}{\numberline {3}Non-Universal Higgs Mass model}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}NUHM Parameter Space}{7}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}RG Equations}{7}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Mass spectrum}{8}{subsection.3.3}
\contentsline {section}{\numberline {4}Sneutrino NLSP properties}{9}{section.4}
\contentsline {subsection}{\numberline {4.1}Gravitino problem + BBN}{9}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Sneutrino Mass}{9}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}$m_{\mathaccentV {tilde}87E\tau }$ vs. $m_{\mathaccentV {tilde}87E\nu }$}{9}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Sneutrino Lifetime}{9}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Sneutrinos in cosmology}{10}{subsection.4.5}
\contentsline {section}{\numberline {5}Sneutrino signals in Colliders}{11}{section.5}
\contentsline {subsection}{\numberline {5.1}Trilepton signatures}{11}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Discuss the background}{11}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Why tau tau lepton signal}{11}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Motivate Cuts}{11}{subsection.5.4}
\contentsline {section}{\numberline {6}Monte Carlo Simulation}{12}{section.6}
\contentsline {subsection}{\numberline {6.1}Matrix element and cross-section calculation}{12}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Event Generators}{12}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Analysis Scheme}{13}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}Validating Simulations}{13}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}Results}{13}{subsection.6.5}
\contentsline {section}{\numberline {7}Machine Learning}{14}{section.7}
\contentsline {subsection}{\numberline {7.1}Deep Learning}{14}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Setup}{14}{subsection.7.2}
\contentsline {section}{\numberline {8}Results}{15}{section.8}
\contentsline {subsection}{\numberline {8.1}Compare to article}{15}{subsection.8.1}
\contentsline {section}{\numberline {9}Conclusion}{16}{section.9}
